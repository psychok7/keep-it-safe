package Common;

//@author psychok7

import IO.OpenObject;
import IO.SaveObject;
import UI.*;
import java.util.*;
import java.math.*;
import java.security.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.IllegalBlockSizeException;


public class Keep_It_Safe {
     private Frame_Main UI_main;
     private Frame_Create_Account UI_create_account;
     private Frame_Login UI_login;
     private Frame_Output UI_output;
     public static OpenObject read;
     public static ArrayList <Account> accounts = new ArrayList <Account>();
     private String output,output2;


    public Keep_It_Safe(Frame_Main UI_main,Frame_Create_Account UI_create_account){
        this.UI_main=UI_main;
        this.UI_create_account=UI_create_account;
    }
    public Keep_It_Safe(Frame_Main UI_main){
        this.UI_main=UI_main;
    }
    public Keep_It_Safe(Frame_Create_Account UI_create_account){
        this.UI_create_account=UI_create_account;
    }
    public Keep_It_Safe(Frame_Login UI_login){
        this.UI_login=UI_login;
    }
    public Keep_It_Safe(Frame_Output UI_output){
        this.UI_output=UI_output;
    }
    public void create_account(){
        Account account = null;
        try {
            account = new Account(this.getUI_create_account().getjTextField1(), this.getUI_create_account().getjPasswordField1(), this.getUI_create_account().getjTextArea1());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Keep_It_Safe.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(Keep_It_Safe.class.getName()).log(Level.SEVERE, null, ex);
        }
        SaveObject save=null;
        /*
        this.read=new OpenObject();
        if(this.read.openfile()){
            this.accounts=this.read.getAccounts();
        }
         * */
        boolean flag=false;
        String pass;
        pass=encrypt_md5(getUI_create_account().getjPasswordField2());  //flag pass again
        if (account.get_password().equals(pass)){
            for (int i=0;i<Keep_It_Safe.accounts.size();i++){
                if((Keep_It_Safe.accounts.get(i).get_name()).equals(account.get_name())){
                   flag=true;
                   break;
                }
            }
            if (flag){
                this.output=printalreadyexists();
            }
            else{
                save=new SaveObject();
                Keep_It_Safe.accounts.add(account);
                this.output=printpasswordcorrect();
                this.output2=save.saveFile(Keep_It_Safe.accounts);
            }
        }
        else{
           this.output=printpasswordincorrect();
        }

    }

    public void SignIn(){
        Keep_It_Safe.read=new OpenObject(getUI_login().getjTextField1());
        Account account=new Account();
        boolean flag=false;
        String pass;
        if(Keep_It_Safe.read.openfile()){
            pass=encrypt_md5(getUI_login().getjPasswordField1());
            for(int i=0;i<Keep_It_Safe.accounts.size();i++){
                account = (((Account) (Keep_It_Safe.accounts.get(i))));
                if ((account.get_name()).equals(getUI_login().getjTextField1())){
                    flag=true;
                    break;
                }
                else{
                    this.output=printuserincorrect();

                }

            }
            if (account.password.equals(pass) && flag==true){
                this.output=printUserInfo(account);
            }
            else if (account.password.equals(pass) && flag==false){
                this.output=printuserincorrect();
            }
            else{
                this.output=printpasswordincorrect();

              }
        }

        else{
            this.output="The Account Does Not Exist.Please Create A New Account";
        }

    }
    public void delete_account (Frame_Main aux){
        Keep_It_Safe.read=new OpenObject(getUI_login().getjTextField1());
        SaveObject save=new SaveObject();
        boolean flag=false;
        Account account=new Account();
        String pass;
        if(Keep_It_Safe.read.openfile()){
            pass=encrypt_md5(getUI_login().getjPasswordField1());
            for(int i=0;i<Keep_It_Safe.accounts.size();i++){
                account = (((Account) (Keep_It_Safe.accounts.get(i))));
                if ((account.get_name()).equals(getUI_login().getjTextField1())){
                    flag=true;
                    break;
                }
                else{
                    this.output=printuserincorrect();

                }

            }
            if (account.password.equals(pass) && flag==true){
                //this.output=printUserInfo(account);
                for(int i=0;i<Keep_It_Safe.accounts.size();i++){
                    account = (((Account) (Keep_It_Safe.accounts.get(i))));
                    if ((account.get_name()).equals(getUI_login().getjTextField1())){
                     Keep_It_Safe.accounts.remove(i);
                     this.output="User Successfully Removed";
                     this.output2=save.saveFile(Keep_It_Safe.accounts);
                     break;
                    }
                    else{
                        this.output=printuserincorrect();
                    }

                }

                if (Keep_It_Safe.accounts.isEmpty()){
                     aux.change_label_false();   //put login invisible if empty
                }

            }
            else if (account.password.equals(pass) && flag==false){
                this.output=printuserincorrect();
            }
            else{
                this.output=printpasswordincorrect();

              }
        }

        else{
            this.output="The Account Does Not Exist.Please Create A New Account";
        }
    }
    /*
    public void delete_all_accounts(){
        SaveObject save=new SaveObject();
        for(int i=0;i<Keep_It_Safe.accounts.size();i++){
            Keep_It_Safe.accounts.remove(i);
        }
        save.saveFile(Keep_It_Safe.accounts);

    }
     */
    public String printpasswordincorrect(){
        String password_incorrect="Password Incorrect,Try Again";
        return password_incorrect;
    }
    public String printpasswordcorrect(){
        String password_correct="Password Correct";
        return password_correct;
    }
    public String printUserInfo(Account account){
        // Create encrypter/decrypter class
        DesEncrypter encrypter = new DesEncrypter(account.key);

        // Decrypt
        String userinfo = encrypter.decrypt(account.info);
        return userinfo;
    }
    public String printuserincorrect(){
        String user_incorrect="This Account Does Not Exist, Please Create A New User";
        return user_incorrect;
    }
    public String printalreadyexists(){
        String password_incorrect="File Already Exists,Use a Diferente Name";
        return password_incorrect;
    }
    public boolean Show_Accounts(){
        Keep_It_Safe.read=new OpenObject();
        boolean flag=false;

        if(Keep_It_Safe.read.openfile())
        {
            flag=true;
        }

        return flag;

    }
    private String encrypt_md5(String info){
        MessageDigest mdEnc=null; // Encryption algorithm
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Keep_It_Safe.class.getName()).log(Level.SEVERE, null, ex);
        }
        mdEnc.update(info.getBytes(), 0, info.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16); // Encrypted string

        return md5;

   }

    /**
     * @return the UI_main
     */
    public Frame_Main getUI_main() {
        return UI_main;
    }

    /**
     * @return the UI_create_account
     */
    public Frame_Create_Account getUI_create_account() {
        return UI_create_account;
    }

    /**
     * @return the UI_login
     */
    public Frame_Login getUI_login() {
        return UI_login;
    }

    /**
     * @return the UI_output
     */
    public Frame_Output getUI_output() {
        return UI_output;
    }

    /**
     * @return the output
     */
    public String getOutput() {
        return output;
    }

    /**
     * @return the output2
     */
    public String getOutput2() {
        return output2;
    }


    

}

