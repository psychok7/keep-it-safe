package Common;


/**
 * @author Nuno Khan
 * wavefile is the name of the wave file you want to play
 */
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.*;
public class Play_Sound extends Thread {
    protected String wavefile;

    public Play_Sound(String wavefile){
        this.wavefile=wavefile;

    }

    public void run (){
        try {
            soundplay(this.wavefile);
        } catch (Exception ex) {
            Logger.getLogger(Play_Sound.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void soundplay(String wavefile) throws Exception {
         File soundFile = new File(wavefile);
         AudioInputStream soundIn = AudioSystem.getAudioInputStream(soundFile);
         AudioFormat format = soundIn.getFormat();
         DataLine.Info info = new DataLine.Info(Clip.class, format);

         Clip clip = (Clip)AudioSystem.getLine(info);
         clip.open(soundIn);
         clip.start();
         while(clip.isRunning())
         {
            Thread.yield();
         }
    
    }

}
