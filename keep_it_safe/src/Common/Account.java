package Common;

import java.io.*;
import java.security.*;
import java.math.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class Account implements Serializable {
    protected String name;
    protected String password;
    protected String info;
    protected SecretKey key;
    
    public Account(String name,String password,String info) throws NoSuchAlgorithmException, IllegalBlockSizeException{
        this.name=name;
        this.password=encrypt_md5(password);
        this.key = KeyGenerator.getInstance("DES").generateKey();

        // Create encrypter/decrypter class
        DesEncrypter encrypter = new DesEncrypter(getKey());
        this.info=encrypter.encrypt(info);
    }
    public Account(){
        
    }
   private String encrypt_md5(String info){
        MessageDigest mdEnc=null; // Encryption algorithm
        try {
            mdEnc = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Keep_It_Safe.class.getName()).log(Level.SEVERE, null, ex);
        }
        mdEnc.update(info.getBytes(), 0, info.length());
        String md5 = new BigInteger(1, mdEnc.digest()).toString(16); // Encrypted string

        return md5;

   }
    
    public String get_name(){
        return this.name;
        
    }
    public String get_password(){
        return this.password;
    }
    public String info(){
        return this.info;
    }

    /**
     * @return the key
     */
    public SecretKey getKey() {
        return key;
    }


}
